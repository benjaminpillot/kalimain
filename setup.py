from setuptools import setup, find_packages

import kalimain

with open("README.md", 'r') as fh:
    long_description = fh.read()

with open("requirements.txt") as req:
    install_req = req.read().splitlines()

setup(name='kalimain',
      version=kalimain.__version__,
      description='Kalimain 2.0',
      long_description=long_description,
      long_description_content_type="text/markdown",
      url='https://framagit.org/benjaminpillot/kalimain',
      author='Benjamin Pillot <benjamin.pillot@ird.fr>',
      install_requires=install_req,
      python_requires='>=3',
      license='The MIT Licence',
      packages=find_packages(),
      zip_safe=False)
