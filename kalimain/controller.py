# -*- coding: utf-8 -*-

""" Module summary description.

More detailed description.
"""
import os
import tkinter as tk
from _tkinter import TclError
from abc import abstractmethod
from pathlib import Path
from tkinter import messagebox, filedialog
from tkinter.ttk import Style

from kalimain import __version__ as __kversion__
from kalimain import __copyright__ as __kcopyright__
from kalimain.buttons import ToggleButtonGroup
from kalimain.controltools import Command, KState
from kalimain.database import MANNING_VALID_NB_POINTS, MLP_VALID_NB_POINTS
from kalimain.dialog import CaveDialog, ImageDialog, NewProjectDialog, OpenProjectDialog, \
    HandDialog, SettingsDialog
from kalimain.observer import Observer


class Controller:

    # Observer classes
    class AddObserver(Observer):
        def __init__(self, view):
            self.view = view

        def update(self, observable, arg):
            self._update(observable, arg)
            self.view.notify_observers()

        @abstractmethod
        def _update(self, observable, arg):
            pass

    def __init__(self, view, model):
        self.model = model
        self.view = view
        self.add_observers_to_notifiers()
        self.add_controls()

    @abstractmethod
    def add_observers_to_notifiers(self):
        pass

    @abstractmethod
    def add_controls(self):
        pass


class MainController(Controller):

    # Controls
    toggle_button_group = None
    mouse_button1 = None
    mouse_motion = None

    ##################
    # Observer classes

    class AddPoint(Controller.AddObserver):

        def _update(self, observable, points):
            self.view.canvas_points.append(self.view.draw_point(points[-1]))
            if len(points) > 1:
                self.view.canvas_lines.append(self.view.draw_line(points[-2:]))

    class AddHand(Controller.AddObserver):

        def _update(self, observable, hand):
            self.view.freeze_hand()
            self.view.add_hand(hand)
            self.view.reset_canvas_objects()

    class DeleteHand(Controller.AddObserver):

        def _update(self, observable, hand_id):
            self.view.delete_hand(hand_id)

    class DeleteLastPoint(Controller.AddObserver):

        def _update(self, observable, arg):
            self.view.delete_last_point()

    class HandInfo(Controller.AddObserver):

        def _update(self, observable, info):
            self.view.disp_manning_info(info)

    class AddImage(Controller.AddObserver):

        def _update(self, observable, image):
            self.view.image_listbox.append(image.name, image.id)

    class DeleteImage(Controller.AddObserver):

        def _update(self, observable, image_id):

            self.view.clear_canvas()
            self.view.image_listbox.drop(image_id)

    class EditImage(Controller.AddObserver):

        def _update(self, observable, image):
            self.view.clear_canvas()
            self.view.image_listbox.drop(image.id)
            self.view.image_listbox.append(image.name, image.id)

    class SetImage(Controller.AddObserver):

        def _update(self, observable, image):
            self.view.clear_canvas()
            self.view.load_canvas(image)

    class AddCave(Controller.AddObserver):

        def _update(self, observable, cave):
            self.view.cave_listbox.append(cave.name, cave.id)

    class DeleteCave(Controller.AddObserver):

        def _update(self, observable, cave_id):
            self.view.clear_canvas()
            self.view.image_listbox.drop_all()
            self.view.cave_listbox.drop(cave_id)

    class EditCave(Controller.AddObserver):

        def _update(self, observable, cave):
            self.view.cave_listbox.drop(cave.id)
            self.view.cave_listbox.append(cave.name, cave.id)

    class SetCave(Controller.AddObserver):

        def _update(self, observable, cave):
            self.view.image_listbox.populate([img.name for img in cave.images],
                                             [img.id for img in cave.images])

    ####################
    # Controller methods
    def _add_commands(self):
        """ Add commands to view buttons

        :return:
        """
        # Left bar buttons
        for i, button in enumerate(self.view.left_bar_buttons):
            button.config(command=self.__getattribute__("on_left_bar_button%d" % i))

        for i, button in enumerate(self.view.left_bar_toggle_buttons):
            button.set_command(self.__getattribute__("on_toggle_button%d" % i))

        # Control panel
        self.view.add_cave_button.config(command=self.on_new_cave)
        self.view.edit_cave_button.config(command=self.on_edit_cave)
        self.view.delete_cave_button.config(command=self.on_delete_cave)
        self.view.add_image_button.config(command=self.on_add_image)
        self.view.add_img_bunch_button.config(command=self.on_add_img_bunch)
        self.view.edit_image_button.config(command=self.on_edit_image)
        self.view.delete_image_button.config(command=self.on_delete_image)

    def _add_update_controls(self):
        """ Add update function rules to buttons

        :return:
        """
        def cliststate():
            return True if self.view.cave_listbox.curselection() else False

        def imgliststate():
            return True if self.view.image_listbox.curselection() else False

        def imgstate():
            return True if self.view.image else False

        def handstate():
            return True if self.view.hands else False

        def undostate():
            return True if self.view.canvas_points else False

        def ptstate():
            if self.model.settings.current_settings.selected_model == "manning":
                return True if len(self.view.canvas_points) in MANNING_VALID_NB_POINTS else False
            elif self.model.settings.current_settings.selected_model == "mlp":
                return True if len(self.view.canvas_points) in MLP_VALID_NB_POINTS else False
            # Other models to be implemented ...
            else:
                return False

        self.view.left_bar_buttons[0].kstate = KState(self.view.left_bar_buttons[0],
                                                      self.view, ptstate)
        self.view.left_bar_buttons[1].kstate = KState(self.view.left_bar_buttons[1],
                                                      self.view, undostate)
        self.view.left_bar_toggle_buttons[0].kstate = KState(self.view.left_bar_toggle_buttons[0],
                                                             self.view, imgstate)
        self.view.left_bar_toggle_buttons[1].kstate = KState(self.view.left_bar_toggle_buttons[1],
                                                             self.view, imgstate)
        self.view.left_bar_toggle_buttons[2].kstate = KState(self.view.left_bar_toggle_buttons[2],
                                                             self.view, handstate)
        self.view.left_bar_toggle_buttons[3].kstate = KState(self.view.left_bar_toggle_buttons[3],
                                                             self.view, handstate)
        self.view.left_bar_toggle_buttons[4].kstate = KState(self.view.left_bar_toggle_buttons[4],
                                                             self.view, handstate)
        self.view.edit_cave_button.kstate = KState(self.view.edit_cave_button,
                                                   self.view.cave_listbox, cliststate)
        self.view.delete_cave_button.kstate = KState(self.view.delete_cave_button,
                                                     self.view.cave_listbox, cliststate)
        self.view.add_image_button.kstate = KState(self.view.add_image_button,
                                                   self.view.cave_listbox, cliststate)
        self.view.add_img_bunch_button.kstate = KState(self.view.add_img_bunch_button,
                                                       self.view.cave_listbox,
                                                       cliststate)
        self.view.edit_image_button.kstate = KState(self.view.edit_image_button,
                                                    self.view.image_listbox, imgliststate)
        self.view.delete_image_button.kstate = KState(self.view.delete_image_button,
                                                      self.view.image_listbox,
                                                      imgliststate)
        for scale in self.view.image_enhance_scale:
            scale.kstate = KState(scale, self.view, imgstate)

    def add_controls(self):
        """ Add controls to widgets

        :return:
        """
        # Toggle controls
        self.toggle_button_group = ToggleButtonGroup(self.view.left_bar_toggle_buttons)
        self.toggle_button_group.cmd = Command(self.view.image_listbox,
                                               self.toggle_button_group.switch_off)

        # Listbox controls
        self.view.cave_listbox.cmd = Command(self.view.cave_listbox,
                                             self.on_select_cave)
        self.view.image_listbox.cmd = Command(self.view.image_listbox,
                                              self.on_select_image)

        # Image enhance controls
        for i, scale in enumerate(self.view.image_enhance_scale):
            scale.config(command=self.__getattribute__("on_scale%d" % i))

        # Add commands to buttons
        self._add_commands()

        # Add update controls
        self._add_update_controls()

    def add_observers_to_notifiers(self):
        """ Add observers to notifiers

        :return:
        """
        self.model.image_model.add_object_notifier.add_observer(
            MainController.AddImage(self.view))
        self.model.image_model.delete_object_notifier.add_observer(
            MainController.DeleteImage(self.view))
        self.model.image_model.edit_object_notifier.add_observer(
            MainController.EditImage(self.view))
        self.model.image_model.set_object_notifier.add_observer(
            MainController.SetImage(self.view))
        self.model.cave_model.add_object_notifier.add_observer(
            MainController.AddCave(self.view))
        self.model.cave_model.delete_object_notifier.add_observer(
            MainController.DeleteCave(self.view))
        self.model.cave_model.edit_object_notifier.add_observer(
            MainController.EditCave(self.view))
        self.model.cave_model.set_object_notifier.add_observer(
            MainController.SetCave(self.view))
        self.model.point_model.add_point_notifier.add_observer(
            MainController.AddPoint(self.view))
        self.model.hand_model.add_object_notifier.add_observer(
            MainController.AddHand(self.view))
        self.model.hand_model.delete_object_notifier.add_observer(
            MainController.DeleteHand(self.view))
        self.model.point_model.delete_last_point_notifier.add_observer(
            MainController.DeleteLastPoint(self.view))
        self.model.hand_model.hand_info_notifier.add_observer(
            MainController.HandInfo(self.view))

    ##################
    # Callback methods
    def on_left_bar_button0(self):
        meta = HandDialog(self.view.root)
        if meta.result:
            self.model.hand_model.add_hand(**meta.result)

    def on_left_bar_button1(self):
        self.model.point_model.delete_last_point()

    def on_toggle_button0(self):
        if self.view.left_bar_toggle_buttons[0].state == 1:
            self.mouse_button1 = self.view.image.canvas.bind("<Button-1>",
                                                             self.on_mouse_button_draw)
        else:
            self.view.image.canvas.unbind("<Button-1>", self.mouse_button1)

    def on_toggle_button1(self):
        if self.view.left_bar_toggle_buttons[1].state == 1:
            self.view.image.bind_mouse_moves()
        else:
            self.view.image.unbind_mouse_moves()

    def on_toggle_button2(self):
        if self.view.left_bar_toggle_buttons[2].state == 1:
            self.mouse_button1 = self.view.image.canvas.bind("<Button-1>",
                                                             self.on_mouse_button_erase)
        else:
            self.view.image.canvas.unbind("<Button-1>", self.mouse_button1)

    def on_toggle_button3(self):
        if self.view.left_bar_toggle_buttons[3].state == 1:
            self.mouse_button1 = self.view.image.canvas.bind("<Button-1>",
                                                             self.on_mouse_button_edit)
        else:
            self.view.image.canvas.unbind("<Button-1>", self.mouse_button1)

    def on_toggle_button4(self):
        if self.view.left_bar_toggle_buttons[4].state == 1:

            self.mouse_motion = self.view.image.canvas.bind("<Motion>",
                                                            self.on_mouse_button_info)
            # self.mouse_button1 = self.view.image.canvas.bind("<Button-1>",
            #                                                  self.on_mouse_button_info)
        else:
            self.view.image.canvas.unbind("<Motion>", self.mouse_motion)
            # self.view.image.canvas.unbind("<Button-1>", self.mouse_button1)

    def on_new_cave(self):
        """ Add cave button callback

        :return:
        """
        meta = CaveDialog(self.view.root,
                          default_name="Cave %d" % (len(self.view.cave_listbox.items) + 1))
        if meta.result:
            self.model.cave_model.add_cave(**meta.result)

    def on_edit_cave(self):
        meta = CaveDialog(self.view.root, title="Edit cave information",
                          default_name=self.model.cave_model.current_object.name,
                          default_description=self.model.cave_model.current_object.description,
                          default_latitude=self.model.cave_model.current_object.latitude,
                          default_longitude=self.model.cave_model.current_object.longitude)
        if meta.result:
            self.model.cave_model.edit_object(self.view.cave_listbox.get_selected_id(),
                                              **meta.result)

    def on_delete_cave(self):
        answer = messagebox.askokcancel("Delete cave",
                                        "Are you sure you want to delete '%s'?" %
                                        self.view.cave_listbox.get_selected_item())
        if answer:
            self.model.cave_model.delete_object(self.view.cave_listbox.get_selected_id())

    def on_add_image(self):
        """ Add image button callback
        """
        # TODO: change initial dir for file dialog
        file = filedialog.askopenfile(initialdir="/home/benjamin/Documents/kalimain/sample",
                                      title='Import image')
        if file:
            meta = ImageDialog(self.view.root,
                               default_name=os.path.splitext(os.path.basename(file.name))[0])
            if meta.result:
                self.model.image_model.add_image(file.name, **meta.result)

    def on_add_img_bunch(self):
        """ Add a bunch of images (located in directory) -- Button callback
        """
        dir_ = filedialog.askdirectory(initialdir="~/Documents",
                                       title="Import directory")

        if dir_:
            directory = Path(dir_).glob('**/*')
            filenames = [dict(filename=str(file), name=os.path.splitext(file.name)[0])
                         for file in directory if file.is_file()
                         and not file.name.startswith(".")]  # Avoid potential hidden files

            for filename in filenames:
                self.model.image_model.add_image(**filename)

    def on_edit_image(self):
        meta = ImageDialog(self.view.root, title="Edit image information",
                           default_name=self.model.image_model.current_object.name,
                           default_description=self.model.image_model.current_object.description)
        if meta.result:
            self.model.image_model.edit_object(self.view.image_listbox.get_selected_id(),
                                               **meta.result)

    def on_delete_image(self):
        answer = messagebox.askokcancel("Delete image",
                                        "Are you sure you want to delete '%s'?" %
                                        self.view.image_listbox.get_selected_item())
        if answer:
            self.model.image_model.delete_object(self.view.image_listbox.get_selected_id())

    ##################
    # Listbox controls
    def on_select_cave(self):
        if self.view.cave_listbox.curselection():
            self.model.cave_model.set_object(self.view.cave_listbox.get_selected_id())

    def on_select_image(self):
        if self.view.image_listbox.curselection():
            self.model.image_model.set_object(self.view.image_listbox.get_selected_id())
        else:
            self.view.clear_canvas()

    ########################
    # Image enhance controls
    def on_scale0(self, _):
        """ Color scale
        """
        self.view.image.enhance(self.view.get_scale_factor())

    def on_scale1(self, _):
        """ Brightness scale
        """
        self.view.image.enhance(self.view.get_scale_factor())

    def on_scale2(self, _):
        """ Contrast scale
        """
        self.view.image.enhance(self.view.get_scale_factor())

    def on_scale3(self, _):
        """ Sharpness scale
        """
        self.view.image.enhance(self.view.get_scale_factor())

    #############
    # Mouse binds
    def on_mouse_button_draw(self, event):
        """ Draw clicked button callback

        Add point when mouse button is clicked and cursor located within image
        :param event:
        :return:
        """
        xy = self.view.containerxy(event)
        if xy:
            self.model.point_model.add_point(x=xy[0], y=xy[1])

    def on_mouse_button_erase(self, event):
        """ Erase clicked button callback

        Erase hand when mouse button is clicked within
        :param event:
        :return:
        """
        hand_id = self.view.get_cursor_hand_id(event)
        if hand_id:
            answer = messagebox.askokcancel("Delete hand",
                                            "Are you sure you want to delete this hand?")
            if answer:
                self.model.hand_model.delete_object(hand_id)

    def on_mouse_button_edit(self, event):
        """ Edit clicked button callback

        :param event:
        :return:
        """
        hand_id = self.view.get_cursor_hand_id(event)
        if hand_id:
            self.model.hand_model.set_object(hand_id)
            meta = HandDialog(self.view.root, title="Edit hand metadata",
                              default_hand_side=self.model.hand_model.current_object.hand_side,
                              default_panel_nb=self.model.hand_model.current_object.panel_nb,
                              default_hand_nb=self.model.hand_model.current_object.hand_nb,
                              default_description=self.model.hand_model.current_object.description)
            if meta.result:
                self.model.hand_model.edit_object(hand_id, **meta.result)

    def on_mouse_button_info(self, event):
        """ Get info about hands when moving over with the cursor

        :return:
        """
        hand_id = self.view.get_cursor_hand_id(event)
        if hand_id:
            self.model.hand_model.get_hand_info(hand_id)
        else:
            self.view.manning_canvas.itemconfigure(self.view.manning_display, text="")


class DataDisplayController(Controller):

    def add_controls(self):
        pass

    def add_observers_to_notifiers(self):
        pass


class KController(Controller):

    tab_controllers = (MainController, DataDisplayController)

    # Observer classes
    class AddProject(Controller.AddObserver):

        def _update(self, observable, project):
            # self.view.projects.append(project.name)
            # self.view.project_ids.append(project.id)
            pass

    class SetProject(Controller.AddObserver):

        def _update(self, observable, project):
            """ Update all observers when project is set

            :param observable:
            :param project:
            :return:
            """
            # Set project name
            self.view.project_label.config(text="Project: %s" % project.name)

            # Clear canvas in Main View
            self.view.views[0].clear_canvas()

            # TODO: update corresponding display in DataDisplay panel

            # Populate cave listbox
            self.view.views[0].cave_listbox.populate([cave.name for cave in project.caves],
                                                     [cave.id for cave in project.caves])

            # Remove images from corresponding listbox
            self.view.views[0].image_listbox.delete(0, tk.END)

            # Activate menu entry for exporting to csv ("File")
            self.view.menu_bar[0].entryconfig("Export to csv", state=tk.NORMAL)

    class DeleteProject(Controller.AddObserver):

        def _update(self, observable, project):

            pass

    def __init__(self, view, model):
        """ Build KController class instance

        """
        super().__init__(view, model)

        # Sub controllers
        self.controllers = [control(view, self.model)
                            for control, view in zip(self.tab_controllers, self.view.views)]

    def run(self):
        self.view.root.style = Style()
        self.view.root.style.theme_use("clam")
        self.view.root.title("Kalimain")
        self.view.root.deiconify()
        self.view.root.mainloop()

    def add_controls(self):
        """ Add controls

        :return:
        """
        def is_project_set():
            return True if self.view.project_label.cget("text") else False

        # State of "add cave" button depends on whether project is set or not
        self.view.views[0].add_cave_button.kstate = KState(self.view.views[0].add_cave_button,
                                                           self.view, is_project_set)

        # Add controls to Menu bar
        for menu in self.view.menu_bar:
            for item in range(menu.index("end") + 1):
                try:
                    entry = menu.entrycget(item, "label").lower()
                    if entry == "new project":
                        menu.entryconfig(item, command=self.on_new_project)
                    elif entry == "open project":
                        menu.entryconfig(item, command=self.on_open_project)
                    elif entry == "delete project":
                        menu.entryconfig(item, command=self.on_delete_project)
                    elif entry == "settings":
                        menu.entryconfig(item, command=self.on_settings)
                    elif entry == "export to csv":
                        menu.entryconfig(item, command=self.on_export_to_csv, state='disabled')
                    elif entry == "exit":
                        menu.entryconfig(item, command=self.on_close)
                    elif entry == "about":
                        menu.entryconfig(item, command=self.on_about_menu)
                except TclError:  # To avoid separators in Menu entrycget
                    pass

    def add_observers_to_notifiers(self):

        self.model.project_model.add_object_notifier.add_observer(
            KController.AddProject(self.view))
        self.model.project_model.set_object_notifier.add_observer(
            KController.SetProject(self.view))
        self.model.project_model.delete_object_notifier.add_observer(
            KController.DeleteProject(self.view))

    @staticmethod
    def on_about_menu():
        messagebox.showinfo("About", message="Kalimain %s\n%s" % (__kversion__, __kcopyright__))

    def on_close(self):
        # TODO: close all files explicitly that are opened before closing the main window
        self.view.root.destroy()

    def on_reset(self):
        pass

    def on_export_to_csv(self):
        file = filedialog.asksaveasfilename(defaultextension=".csv", title="Export to csv")
        if file:
            self.model.export_to_csv(file, "hands")

    def on_delete_project(self):
        w_dialog = OpenProjectDialog(self.view.root,
                                     self.model.project_model.projects,
                                     "Delete project")
        if w_dialog.result:
            if w_dialog.result["project_name"] == self.view.current_project:
                messagebox.showwarning("Delete not permitted",
                                       "'%s' is the current project." %
                                       w_dialog.result["project_name"])
            else:
                answer = messagebox.askokcancel("Delete project",
                                                "Are you sure you want to delete '%s'?" %
                                                w_dialog.result["project_name"])
                if answer:
                    self.model.project_model.delete_object(w_dialog.result["project_id"])

    def on_new_project(self):
        w_dialog = NewProjectDialog(self.view.root,
                                    title="New project",
                                    default_name="Project %d" % (len(
                                        self.model.project_model.projects) + 1))
        if w_dialog.result:
            self.model.project_model.add_project(**w_dialog.result)
            self.model.project_model.set_object(self.model.project_model.projects[-1].id)
            self.view.current_project = w_dialog.result["name"]

    def on_open_project(self):
        w_dialog = OpenProjectDialog(self.view.root, self.model.project_model.projects)

        if w_dialog.result:
            self.model.project_model.set_object(w_dialog.result["project_id"])
            self.view.current_project = w_dialog.result["project_name"]

    def on_settings(self):
        w_dialog = SettingsDialog(self.view.root, self.model.settings.current_settings)

        if w_dialog.result:
            self.model.settings.update_settings(w_dialog.result)
