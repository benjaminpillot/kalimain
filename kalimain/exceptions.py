# -*- coding: utf-8 -*-

""" Module main description

More detailed description.
"""
import os
import traceback
from tkinter import messagebox

from kalimain import kalimain_home_directory


class KCatcher:
    """ Catch GUI exceptions

    """
    def __init__(self, func, subst, widget):
        self.func = func
        self.subst = subst
        self.widget = widget

    def __call__(self, *args):
        try:
            if self.subst:
                args = self.subst(*args)
            return self.func(*args)
        except Exception as e:
            if isinstance(e, Warning):
                pass  # Keep it silent
                # messagebox.showwarning(title=e.__class__.__name__, message=e)
            else:
                with open(os.path.join(kalimain_home_directory, 'kalimain.log'), 'a') as log:
                    traceback.print_exc(file=log)
                messagebox.showerror(title=e.__class__.__name__, message=e)


class KException(Exception):
    pass


class KWarning(Warning):
    pass


class ImageError(KException):
    pass


class EntryError(KException):
    pass


class FloatEntryError(EntryError):
    pass


class DeleteWarning(KWarning):
    pass


class ReadFileWarning(KWarning):
    pass


class ApiConnectionWarning(KWarning):
    pass


class DuplicateElementWarning(KWarning):
    pass


class InvalidNameWarning(KWarning):
    pass
