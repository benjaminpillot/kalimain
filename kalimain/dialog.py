# -*- coding: utf-8 -*-

""" Module for window dialogs

Add here any window dialog necessary to Kalimain
"""
import tkinter as tk

from tkinter import font as tkfont
from tkinter import ttk
from abc import ABCMeta, abstractmethod
from tkinter.ttk import Separator

from kalimain.viewtools import FloatEntry
from kalimain.widgets import KListbox, KScale


class Dialog(tk.Toplevel, metaclass=ABCMeta):
    """ Base class for window dialogs

    Thanks to http://effbot.org/tkinterbook/tkinter-dialog-windows.htm
    """
    font = None
    bold_font = "Helvetica 12 bold"
    font_size = 10
    button_width = 10
    button_padx = 5
    button_pady = 5
    entry_width = 60
    label_width = 20
    label_anchor = 'nw'
    text_height = 10

    def __init__(self, parent, title=None):

        tk.Toplevel.__init__(self, parent)
        self.transient(parent)

        if title:
            self.title(title)

        self.parent = parent

        self.result = None

        body = tk.Frame(self)
        self.initial_focus = self.body(body)
        body.pack(padx=5, pady=5)

        self.buttonbox()

        self.wait_visibility()

        self.grab_set()

        if not self.initial_focus:
            self.initial_focus = self

        self.protocol("WM_DELETE_WINDOW", self.cancel)

        self.initial_focus.focus_set()

        self.wait_window(self)

    #
    # construction hooks

    @abstractmethod
    def body(self, master):
        # create dialog body.  return widget that should have
        # initial focus.  this method should be overridden

        pass

    def buttonbox(self):
        # add standard button box. override if you don't want the
        # standard buttons

        box = tk.Frame(self)

        w = tk.Button(box, text="OK", width=self.button_width, command=self.ok)
        w.pack(side=tk.LEFT, padx=self.button_padx, pady=self.button_pady)
        w = tk.Button(box, text="Cancel", width=self.button_width, command=self.cancel)
        w.pack(side=tk.LEFT, padx=self.button_padx, pady=self.button_pady)

        # self.bind("<Return>", self.ok)
        # self.bind("<Escape>", self.cancel)

        box.pack()

    #
    # standard button semantics

    def ok(self, event=None):

        if not self.validate():
            self.initial_focus.focus_set()  # put focus back
            return

        self.withdraw()
        self.update_idletasks()

        self.apply()

        self.cancel()

    def cancel(self, event=None):

        # put focus back to the parent window
        self.parent.focus_set()
        self.destroy()

    #
    # command hooks

    def validate(self):

        return 1  # override

    @abstractmethod
    def apply(self):

        pass

    @property
    def root_x(self):
        return self.parent.winfo_rootx()

    @property
    def root_y(self):
        return self.parent.winfo_rooty()

    @property
    def root_w(self):
        return self.parent.winfo_width()

    @property
    def root_h(self):
        return self.parent.winfo_height()


class KDialog(Dialog):

    # Entries
    name_entry = None
    description_entry = None

    def __init__(self, parent, title=None, default_name=None, default_description=None):

        self.default_name_entry = default_name
        self.default_description_entry = default_description
        super().__init__(parent, title)

    def body(self, master):
        self.font = tkfont.Font(master, size=self.font_size)

        # Labels
        tk.Label(master, text="Name:", anchor=self.label_anchor, width=self.label_width,
                 font=self.font).grid(row=0, column=0)
        tk.Label(master, text="Description:", anchor=self.label_anchor, width=self.label_width,
                 height=self.text_height, font=self.font).grid(row=1, column=0)

        # Entries
        self.name_entry = tk.Entry(master, width=self.entry_width, bg="white", font=self.font)
        self.description_entry = tk.Text(master,
                                         height=self.text_height,
                                         width=self.entry_width, bg="white",
                                         font=self.font)
        self.name_entry.grid(row=0, column=1)
        self.description_entry.grid(row=1, column=1)

        if self.default_name_entry:
            self.name_entry.insert(0, self.default_name_entry)

        if self.default_description_entry:
            self.description_entry.insert("1.0", self.default_description_entry)

    def apply(self):
        name = self.name_entry.get().strip()
        if name:
            self.result = dict(name=name,
                               description=self.description_entry.get("1.0", "end-1c"))


class HandDialog(Dialog):

    hand_side = None
    panel_nb_entry = None
    hand_nb_entry = None
    description_entry = None
    entry_width = 40
    text_height = 5

    hand_side_value = dict(right=0, left=1)

    def __init__(self, parent, title="Add hand metadata", default_hand_side='right',
                 default_panel_nb=None, default_hand_nb=None, default_description=None):
        self.default_hand_side_entry = default_hand_side
        self.default_panel_nb_entry = default_panel_nb
        self.default_hand_nb_entry = default_hand_nb
        self.default_description_entry = default_description
        super().__init__(parent, title)

    def body(self, master):
        self.font = tkfont.Font(master, size=self.font_size)
        tk.Label(master, text="Hand side:", anchor=self.label_anchor,
                 width=self.label_width, font=self.font).grid(row=0, column=0)
        tk.Label(master, text="Panel number:", anchor=self.label_anchor,
                 width=self.label_width, font=self.font).grid(row=1, column=0)
        tk.Label(master, text="Hand number:", anchor=self.label_anchor,
                 width=self.label_width, font=self.font).grid(row=2, column=0)
        tk.Label(master, text="Description:", anchor=self.label_anchor,
                 width=self.label_width, height=self.text_height, font=self.font).grid(row=3,
                                                                                       column=0)

        self.panel_nb_entry = tk.Entry(master, width=self.entry_width,
                                       bg="white", font=self.font)
        self.hand_nb_entry = tk.Entry(master, width=self.entry_width,
                                      bg="white", font=self.font)
        self.description_entry = tk.Text(master, height=self.text_height, width=self.entry_width,
                                         bg="white", font=self.font)
        self.hand_side = ttk.Combobox(master, width=self.entry_width, font=self.font,
                                      state="readonly", values=['right', 'left'])
        self.hand_side.current(self.hand_side_value[self.default_hand_side_entry])

        self.hand_side.grid(row=0, column=1, columnspan=1, sticky="ENSW")
        self.panel_nb_entry.grid(row=1, column=1, columnspan=1, sticky="ENSW")
        self.hand_nb_entry.grid(row=2, column=1, columnspan=1, sticky="ENSW")
        self.description_entry.grid(row=3, column=1, columnspan=1, sticky="ENSW")

        if self.default_panel_nb_entry:
            self.panel_nb_entry.insert(0, self.default_panel_nb_entry)

        if self.default_hand_nb_entry:
            self.hand_nb_entry.insert(0, self.default_hand_nb_entry)

        if self.default_description_entry:
            self.description_entry.insert("1.0", self.default_description_entry)

    def apply(self):
        self.result = dict(hand_side=self.hand_side.get(),
                           panel_nb=self.panel_nb_entry.get(),
                           hand_nb=self.hand_nb_entry.get(),
                           description=self.description_entry.get("1.0", "end-1c"))


class ImageDialog(KDialog):
    """ Image's window dialog

    """
    def __init__(self, parent, title="Add image", default_name=None, default_description=None):
        super().__init__(parent, title=title, default_name=default_name,
                         default_description=default_description)


class CaveDialog(KDialog):
    """ Cave's window dialog for cave's metadata implementation

    """
    latitude_entry = None
    longitude_entry = None

    def __init__(self, parent, title="New cave", default_name=None, default_description=None,
                 default_latitude=None, default_longitude=None):
        self.default_latitude_entry = default_latitude
        self.default_longitude_entry = default_longitude
        super().__init__(parent, title=title, default_name=default_name,
                         default_description=default_description)

    def body(self, master):
        super().body(master)

        tk.Label(master, text="Latitude:", anchor=self.label_anchor,
                 width=self.label_width, font=self.font).grid(row=2)
        tk.Label(master, text="Longitude:", anchor=self.label_anchor,
                 width=self.label_width, font=self.font).grid(row=3)

        self.latitude_entry = FloatEntry(master, width=self.entry_width, bg="white")
        self.longitude_entry = FloatEntry(master, width=self.entry_width, bg="white")

        self.latitude_entry.grid(row=2, column=1)
        self.longitude_entry.grid(row=3, column=1)

        if self.default_latitude_entry:
            self.latitude_entry.set(self.default_latitude_entry)

        if self.default_longitude_entry:
            self.longitude_entry.set(self.default_longitude_entry)

        return self.name_entry  # initial focus

    def apply(self):
        super().apply()
        if self.result:
            self.result.update(latitude=self.latitude_entry.get(),
                               longitude=self.longitude_entry.get())


class NewProjectDialog(KDialog):
    """ Project's dialog when user creates new project

    """
    pass


class OpenProjectDialog(Dialog):
    """ Project's dialog when user opens existing project

    """
    listbox_height = 20

    klistbox = None
    items = None

    def __init__(self, parent, items, title="Open Project"):
        self.items = items
        self.listbox_width = (self.button_width + self.button_padx) * 2
        super().__init__(parent, title)

    def body(self, master):
        super().body(master)

        self.klistbox = KListbox(master, width=self.listbox_width, height=self.listbox_height)
        self.klistbox.populate([item.name for item in self.items], [item.id for item in self.items])
        self.klistbox.pack(side="top", expand="yes", fill="both")

    def apply(self):
        if self.klistbox.curselection():
            self.result = dict(project_id=self.klistbox.get_selected_id(),
                               project_name=self.klistbox.get_selected_item())


class SettingsDialog(Dialog):
    """ Settings' dialog

    """

    hand_display = None
    gender_shape_radius = None
    man_max_manning = None
    woman_min_manning = None
    model_options = ["manning", "mlp"]
    selected_model = None

    def __init__(self, parent, in_settings):

        self.in_settings = in_settings
        super().__init__(parent, "Settings")

    def body(self, master):

        def select_model():
            self.selected_model = self.model_options[model.get()]

        tk.Label(master, text="Model", anchor=self.label_anchor,
                 width=self.label_width, font=self.bold_font).grid(row=1, column=0)
        Separator(master, orient=tk.HORIZONTAL).grid(row=4, column=0,
                                                     columnspan=2, pady=1, sticky=tk.NSEW)
        tk.Label(master, text="Display", anchor=self.label_anchor,
                 width=self.label_width, font=self.bold_font).grid(row=5, column=0)
        tk.Label(master, text="Hand drawing", anchor=self.label_anchor,
                 width=self.label_width, font=self.font).grid(row=7, column=0)
        tk.Label(master, text="Gender shape radius", anchor=self.label_anchor,
                 width=self.label_width, font=self.font).grid(row=8, column=0)
        tk.Label(master, text="Man max Manning", anchor=self.label_anchor,
                 width=self.label_width, font=self.font).grid(row=9, column=0)
        tk.Label(master, text="Woman min Manning", anchor=self.label_anchor,
                 width=self.label_width, font=self.font).grid(row=10, column=0)

        model = tk.IntVar()
        model_option_1 = tk.Radiobutton(master, text="Manning",
                                        variable=model, value=0, command=select_model)
        model_option_2 = tk.Radiobutton(master, text="MLP Nelson",
                                        variable=model, value=1, command=select_model)
        # Set model
        model.set(self.model_options.index(self.in_settings.selected_model))
        select_model()

        # Scale bars
        self.gender_shape_radius = KScale(master,
                                          self.in_settings.gender_shape_radius,
                                          from_=0, to=50,
                                          resolution=1, orient=tk.HORIZONTAL)
        self.man_max_manning = KScale(master,
                                      self.in_settings.man_max_manning,
                                      from_=0.8, to=1.0,
                                      resolution=0.01, orient=tk.HORIZONTAL)
        self.woman_min_manning = KScale(master,
                                        self.in_settings.woman_min_manning,
                                        from_=1.0, to=1.2,
                                        resolution=0.01, orient=tk.HORIZONTAL)

        # Check box
        self.hand_display = tk.IntVar()
        self.hand_display.set(self.in_settings.hand_display)
        display_hand_check = tk.Checkbutton(master, onvalue=1,
                                            offvalue=0, variable=self.hand_display)

        # Widget placement
        model_option_1.grid(row=2, column=0, sticky=tk.W)
        model_option_2.grid(row=3, column=0, sticky=tk.W)
        display_hand_check.grid(row=7, column=1)
        self.gender_shape_radius.grid(row=8, column=1)
        self.man_max_manning.grid(row=9, column=1)
        self.woman_min_manning.grid(row=10, column=1)

    def apply(self):
        self.result = dict(gender_shape_radius=self.gender_shape_radius.get(),
                           man_max_manning=self.man_max_manning.get(),
                           woman_min_manning=self.woman_min_manning.get(),
                           hand_display=self.hand_display.get(),
                           selected_model=self.selected_model)
