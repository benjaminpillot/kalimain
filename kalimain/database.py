# -*- coding: utf-8 -*-

""" Module summary description.

More detailed description.
"""
# import country_converter as coco
import os
import warnings

from geopy import Nominatim
from geopy.exc import GeocoderServiceError, GeocoderUnavailable
from math import sqrt
from PIL import Image as PilImage
from shapely.affinity import scale
from shapely.geometry import Polygon, LineString
from sqlalchemy import Column, Integer, ForeignKey, Float, String
from sqlalchemy.ext.declarative import declared_attr, declarative_base
from sqlalchemy.orm import relationship

from kalimain.exceptions import ImageError, ApiConnectionWarning


MANNING_MIN_NB_POINTS = 12
MANNING_MAX_NB_POINTS = 15
MANNING_VALID_NB_POINTS = [MANNING_MIN_NB_POINTS, MANNING_MAX_NB_POINTS]
MLP_MIN_NB_POINTS = 14
# MLP_MAX_NB_POINTS = 17
# MLP_VALID_NB_POINTS = [MLP_MIN_NB_POINTS, MLP_MAX_NB_POINTS]
MLP_VALID_NB_POINTS = [MLP_MIN_NB_POINTS]
PALM_POINTS = [0, 1, 3, 4, 6, 8, 10, 12, 13]
# PALM_MAX_POINTS = [0, 1, 3, 4, 6, 7, 9, 10, 12, 13, 15, 16]
DESCRIPTION_LENGTH = 500
NAME_LENGTH = 100
PATH_LENGTH = 300


class Base:
    @declared_attr
    def __tablename__(self):
        return self.__name__.lower() + "s"

    id = Column(Integer, primary_key=True)


Base = declarative_base(cls=Base)


class HPoint(Base):
    """ Point class for storing hand canvas_points

    """
    x = Column(Integer)
    y = Column(Integer)
    hand_id = Column(Integer, ForeignKey('hands.id'))

    hand = relationship("Hand", back_populates="hpoints")


class Hand(Base):
    """ Hand class for storing hands

    """

    # left = Column(Boolean)
    # right = Column(Boolean)
    D1 = Column(Float)
    D2 = Column(Float)
    D3 = Column(Float)
    D4 = Column(Float)
    D5 = Column(Float)
    manning = Column(Float)
    hand_side = Column(String(10))
    panel_nb = Column(String(10))
    hand_nb = Column(String(10))
    description = Column(String(DESCRIPTION_LENGTH))
    image_id = Column(Integer, ForeignKey('images.id'))

    image = relationship("Image", back_populates="hands")
    hpoints = relationship("HPoint", order_by=HPoint.id, back_populates="hand",
                           cascade="all, delete-orphan")

    def __init__(self, list_of_points, **kwargs):
        super().__init__(**kwargs)
        self.hpoints = list_of_points
        self.D1, self.D2, self.D3, self.D4, self.D5 = self.finger_heights()
        self.manning = self.manning_index()

    def get_info(self):
        """ Return a dict of the hand's main info and features

        :return:
        """
        return dict(d1=self.D1, d2=self.D2, d3=self.D3, d4=self.D4, d5=self.D5,
                    manning=self.manning)

    @staticmethod
    def distance(pt1, pt2):
        # Do not return numpy float to avoid error when inserting into the database
        return float(sqrt((pt2.x - pt1.x) ** 2 + (pt2.y - pt1.y) ** 2))

    @staticmethod
    def height_of_finger(start, mid, end):
        """ Return height of finger based on 3 canvas_points

        Update 26/04/2021 : use the finger basis mid point to measure finger height (distance
        between that point and the tip mid point of the current finger)

        :return:
        """
        return Hand.distance(Hand.mid_point(start, end), mid)
        # return 2 * Polygon([(start.x, start.y), (mid.x, mid.y),
        #                     (end.x, end.y)]).area / Hand.distance(start, end)

    @staticmethod
    def mid_point(start, end):
        return HPoint(x=(start.x + end.x)/2, y=(start.y + end.y)/2)

    def finger_heights(self):
        """ Get height of fingers

        :return:
        """
        return [self.height_of_finger(*self.hpoints[i - 1:i + 2]) for i in self.finger_order]

    def is_left_handed(self):
        """ Is hand left or right ?

        Compute width of first and last finger
        :return:
        """
        if self.distance(self.hpoints[0],
                         self.hpoints[2]) < self.distance(self.hpoints[-1],
                                                          self.hpoints[-3]):
            return True
        else:
            return False

    def manning_index(self):
        """ Compute Manning index

        Compute Manning index (ratio 2-Digit/4-Digit)
        :return:
        """
        return self.D2 / self.D4

    @property
    def central_palm_point(self):
        middle_point = self.middle_finger_point
        centroid = self.palm.centroid
        lateral_edge_point = self.lateral_edge_point
        # m =


    @property
    def finger_order(self):
        if len(self.hpoints) == MANNING_MIN_NB_POINTS:
            return [1, 4, 6, 8, 10]  # 12 points
        elif len(self.hpoints) == MANNING_MAX_NB_POINTS:
            return [1, 4, 7, 10, 13]  # 15 points
        else:
            pass  # Different number of points ?

    @property
    def full_hand(self):
        return Polygon([(hpoint.x, hpoint.y) for hpoint in self.hpoints])

    @property
    def lateral_edge(self):
        return LineString([(self.hpoints[i].x, self.hpoints[i].y)
                           for i in [0, 1, 2]])

    @property
    def lateral_edge_point(self):
        straight_line = scale(LineString([(self.hpoints[i].x, self.hpoints[i].y)
                                          for i in [3, 6]]), 10, 10)

        point = straight_line.intersection(self.lateral_edge)

        return HPoint(x=point.x, y=point.y)

    @property
    def medial_edge(self):
        return LineString([(self.hpoints[i].x, self.hpoints[i].y)
                           for i in [11, 12, 13]])

    @property
    def medial_edge_point(self):
        pass

    @property
    def middle_finger_point(self):
        # if len(self.hpoints) == MLP_MIN_NB_POINTS:
        return self.mid_point(self.hpoints[6], self.hpoints[8])
        # elif len(self.hpoints) == MLP_MAX_NB_POINTS:
        #     return self.mid_point(self.hpoints[7], self.hpoints[9])
        # else:
        #     pass

    @property
    def palm(self):
        # if len(self.hpoints) == MLP_MIN_NB_POINTS:
        return Polygon([(self.hpoints[i].x, self.hpoints[i].y)
                        for i in PALM_POINTS])
        # elif len(self.hpoints) == MLP_MAX_NB_POINTS:
        #     return Polygon([(self.hpoints[i].x, self.hpoints[i].y)
        #                     for i in PALM_MAX_POINTS])
        # else:
        #     pass

    @property
    def proximo_medial_point(self):
        straight_line = scale(LineString([(self.hpoints[i].x, self.hpoints[i].y)
                                          for i in [6, 8]]), 10, 10)

        point = straight_line.intersection(self.medial_edge)

        return HPoint(x=point.x, y=point.y)


class Image(Base):
    """ Image class instance for storing image of hands

    """
    name = Column(String(NAME_LENGTH))
    description = Column(String(DESCRIPTION_LENGTH))
    path = Column(String(PATH_LENGTH))
    width = Column(Integer)
    height = Column(Integer)
    cave_id = Column(Integer, ForeignKey("caves.id"))

    cave = relationship("Cave", back_populates="images")
    hands = relationship("Hand", order_by=Hand.id, back_populates="image",
                         cascade="all, delete-orphan")

    def __init__(self, filename, **kwargs):
        super().__init__(**kwargs)

        try:
            # self._image = PilImage.open(filename)
            with PilImage.open(filename) as image:
                self.path = filename
                self.width = image.width
                self.height = image.height
        except (FileNotFoundError, OSError):
            raise ImageError("Unable to read file '%s" % filename)
        # else:
        #     self.path = filename
        #     self.width = self._image.width
        #     self.height = self._image.height

        # Close image file
        # self._image.close()

    def __eq__(self, other):
        if not isinstance(other, Image):
            return False
        if self.size == other.size and os.stat(self.path).st_size == os.stat(other.path).st_size:
            return True
        else:
            return False

    @property
    def size(self):
        return self.width, self.height


class Cave(Base):
    """ Cave class instance for storing caves to which belong images and hands

    """
    # Primary attributes
    name = Column(String(NAME_LENGTH))
    description = Column(String(DESCRIPTION_LENGTH))
    latitude = Column(Float)
    longitude = Column(Float)
    project_id = Column(Integer, ForeignKey("projects.id"))

    # Secondary attributes (retrieved using geopy library)
    city = Column(String(50))
    town = Column(String(50))
    village = Column(String(50))
    suburb = Column(String(50))
    country = Column(String(50))
    country_code = Column(String(2))
    county = Column(String(50))
    state = Column(String(50))
    address = Column(String(200))
    # continent = Column(String(50))

    # Relationships
    project = relationship("Project", back_populates="caves")
    images = relationship("Image", order_by=Image.id, back_populates="cave",
                          cascade="all, delete-orphan")

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.update_dynamic_fields()

    def __eq__(self, other):
        if not isinstance(other, Cave):
            return False
        if self.name == other.name:
            return True
        else:
            return False
    
    def update_dynamic_fields(self):
        try:
            geolocator = Nominatim(user_agent="Kalimain")
            location = geolocator.reverse("%f, %f" % (self.latitude, self.longitude))
        except (GeocoderServiceError, GeocoderUnavailable):
            pass
            # TODO: when warning is launched, case is not set.... Not sure why
            # warnings.warn("Unable to reach service: only primary attributes will be set",
            #               ApiConnectionWarning)
        else:
            self.address = location.raw["display_name"]

            for key, val in location.raw["address"].items():
                try:
                    self.__setattr__(key, val)
                except AttributeError:
                    pass

            # if self.country is not None:
            #     self.continent = coco.convert(self.country, to='Continent')


class Project(Base):
    """ Project class instance

    """
    name = Column(String(50))
    description = Column(String(1000))

    caves = relationship("Cave", order_by=Cave.id, back_populates="project",
                         cascade="all, delete-orphan")

    def __eq__(self, other):
        if not isinstance(other, Project):
            return False
        if self.name == other.name:
            return True
        else:
            return False


class Settings(Base):
    """Settings class instance
    """
    selected_model = Column(String(50))

    # Display
    hand_display = Column(Integer())
    gender_shape_radius = Column(Integer())
    man_max_manning = Column(Float())
    woman_min_manning = Column(Float())
